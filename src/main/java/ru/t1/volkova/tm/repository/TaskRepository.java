package ru.t1.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll(userId)
                .stream()
                .filter(t -> t.getProjectId() != null && projectId.equals(t.getProjectId()))
                .collect(Collectors.toList());
    }

}

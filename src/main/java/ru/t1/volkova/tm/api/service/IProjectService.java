package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Project;

@SuppressWarnings("UnusedReturnValue")
public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @NotNull String description);

    @NotNull
    Project updateByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @Nullable String name,
            @NotNull String description);

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status
    );

    @NotNull
    Project changeProjectStatusByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @NotNull Status status
    );

}

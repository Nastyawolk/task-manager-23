package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Change password of current user";

    @NotNull
    private static final String NAME = "change-user-password";

    @Override
    public void execute() {
        @NotNull final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

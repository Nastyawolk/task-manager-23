package ru.t1.volkova.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(Status status);

}

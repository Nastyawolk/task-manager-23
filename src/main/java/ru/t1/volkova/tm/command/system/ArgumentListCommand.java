package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.model.ICommand;
import ru.t1.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-arg";

    @NotNull
    private static final String DESCRIPTION = "Show arguments list.";

    @NotNull
    private static final String NAME = "arguments";

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            @NotNull final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}

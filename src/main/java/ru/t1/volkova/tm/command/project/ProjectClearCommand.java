package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    private static final String NAME = "project-clear";

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final String userId = getUserId();
        projectService().removeAll(userId);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}

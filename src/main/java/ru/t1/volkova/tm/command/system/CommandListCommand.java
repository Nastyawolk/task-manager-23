package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.model.ICommand;
import ru.t1.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-cmd";

    @NotNull
    private static final String DESCRIPTION = "Show commands list.";

    @NotNull
    private static final String NAME = "commands";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            @NotNull final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}

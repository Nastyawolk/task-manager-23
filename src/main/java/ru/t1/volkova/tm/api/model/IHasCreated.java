package ru.t1.volkova.tm.api.model;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasCreated {

    @NotNull
    Date getCreated();

    void setCreated(Date created);

}

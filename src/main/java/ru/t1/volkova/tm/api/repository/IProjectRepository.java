package ru.t1.volkova.tm.api.repository;

import ru.t1.volkova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}

package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.enumerated.ProjectSort;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Show list projects.";

    @NotNull
    private static final String NAME = "project-list";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        @NotNull final List<Project> projects;
        if (sort == null) projects = projectService().findAll(userId);
        else projects = projectService().findAll(userId, sort.getComparator());
        renderProjects(projects);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
